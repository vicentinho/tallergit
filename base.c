//Vicente Eduardo González Contreras
//18105093-4

#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"
#include <math.h>
//----------------Librerías----------------------------
float desvStd(estudiante curso[],float prom[], int promedio);
float menor(float prom[]);
float mayor(float prom[]);
void registroCurso(estudiante curso[]);
void clasificarEstudiantes(char path[], estudiante curso[]);
void metricasEstudiantes(estudiante curso[], prom[]);
void menu(estudiante curso[]);
//---------------Prototipo de funciones-----------------
float desvStd(estudiante curso[],float prom[], int promedio)
{
	int i;
	float desviacion=0, cuadrados=0,diferencia=0,suma=0,total=0;
	for(i=0; i<23 ; i++){
		{
			diferencia=promedio[i] - promedio;
			cuadrados=pow(diferencia,2);
			suma= suma + cuadrados;
		}
	}
total=(suma/23)- 1;
desviacion= pow(total, 0.5);
return desviacion;
}

float menor(float prom[])
{
	int i=0;
	int posicion=0;
	while(i<23)
	{
		if(prom[i]>prom[i+1])
		{
			posicion=prom[i+1]
			return posicion;
		}
	if(i!=23)
	{
		i=i+1;
	}
	else
	{
		break;
	}
	}
}

float mayor(float prom[])
{
	int i=0;
	int posicion=0;
	while (i<23)
	{
		if(prom[i]<prom[i+1])
		{
			posicion=prom[i];
			return posicion;
		}
	if(i!=23)
	{
		i=i+1;
	}
	else
	{
		break;
	}
	}
}

void registroCurso(estudiante curso[]){
	printf("\nIngreso De Notas Estudiantes: \n");
	int i=0;
	float promedio=0;
	while (i <23) 
	{
		printf("Estudiante: %s %s %s\n", curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM);
		printf("Proyeto 1");
		scanf("%.2f" ,&curso[i].asig_1.proy1);
		printf("Proyeto 2");
		scanf("%.2f" ,&curso[i].asig_1.proy2);
		printf("Proyeto 3");
		scanf("%.2f" ,&curso[i].asig_1.proy3);
		printf("Control 1");
		scanf("%.2f" ,&curso[i].asig_1.cont1);
		printf("Control 2");
		scanf("%.2f" ,&curso[i].asig_1.cont2);
		printf("Control 3");
		scanf("%.2f" ,&curso[i].asig_1.cont3);
		printf("Control 4");
		scanf("%.2f" ,&curso[i].asig_1.cont4);
		printf("Control 5");
		scanf("%.2f" ,&curso[i].asig_1.cont5);
		printf("Control 6");
		scanf("%.2f" ,&curso[i].asig_1.cont6);
		promedio=((curso[i].asig_1.proy1+ curso[i].asig_1.proy2+ curso[i].asig_1.proy3+ curso[i].asig_1.cont1+ curso[i].asig_1.cont2+ curso[i].asig_1.cont3+ curso[i].asig_1.cont4 +curso[i].asig_1.cont5+curso[i].asig_1.cont6)/9);
		printf("El promedio es:%.2f",promedio);
		prom[i]=promedio;
		i=i+1;
	}
}

void clasificarEstudiantes(char path[], estudiante curso[])
{
	
	int i=0;
	int contador=0;
	FILE *sobrecuatro;
	FILE *bajocuatro;
	sobrecuatro=fopen("Aprobados","w");
	bajocuatro=fopen("Reprobados.txt","w");
	while (i<23){
		if (prom[i]>40){
			fprintf(sobrecuatro, "%s %s %s",curso[i].nombre,curso[i].apellidoP,curso[i].apellidoM);
			contador=contador+1;
		}
		else
		{
		fprintf(bajocuatro, "%s %s %s",curso[i].nombre,curso[i].apellidoP,curso[i].apellidoM);
		contador=contador+1;
		}
	i=i+1;
	}
	fprintf("Aprobaron un total de:%d",contador);
	fprintf("Reprobaron un total de:%d",contador);
	fclose(sobrecuatro);
	fclose(bajocuatro);
	
}

void metricasEstudiantes(estudiante curso[], prom[]){
	
	float prom[i]=promedio;
	int=0;
	while (i<23)
	{
		mayor(prom);
		menor(prom);
		desvStd(estudiante curso);
		i=i+1;
	}
	printf("El promedio más bajo es de : %s %s %s",curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM,"con promedio:%2.f",menor(prom));
	printf("El promedio más alto es de : %s %s %s",curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM,"con promedio:%2.f",mayor(prom));
	printf("\n La desviacion estandar es: %.2f",desvStd(estudiante curso,prom));
}
void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
            case 5: clasificarEstudiantes("destino", curso); // clasi
            		break;
         }

    } while ( opcion != 6 );
}
//------------------Funciones---------------------
int main(){
	estudiante curso[23]; 
	float prom[23];
	menu(curso); 
	return EXIT_SUCCESS; 
}
//--------------Cuerpo Main-----------------------